FROM ubuntu:14.04

MAINTAINER feixu feixu@alauda.io

RUN apt-get update && apt-get install -y \
    libpcre3 libpcre3-dev \
    build-essential zlib1g-dev unzip \
    liblua5.1-0-dev liblua50-dev liblualib50-dev luajit \
    wget vim-tiny \
    curl

WORKDIR /prometheus-nginx

RUN cd /root \
    && wget https://github.com/simpl/ngx_devel_kit/archive/v0.3.0.tar.gz \
    && tar -xvzf v0.3.0.tar.gz \
    && wget https://github.com/openresty/lua-nginx-module/archive/v0.10.7.tar.gz \
    && tar -xvzf v0.10.7.tar.gz \
    && wget http://nginx.org/download/nginx-1.11.2.tar.gz \
    && tar -xvzf nginx-1.11.2.tar.gz \
    && cd /root/nginx-1.11.2 \
    && ./configure --prefix=/etc/nginx --sbin-path=/usr/sbin/nginx \
    --conf-path=/etc/nginx/nginx.conf --error-log-path=/var/log/nginx/error.log \
    --http-log-path=/var/log/nginx/access.log  --pid-path=/var/run/nginx.pid  \
    --lock-path=/var/run/nginx.lock --http-client-body-temp-path=/var/cache/nginx/client_temp \
    --http-proxy-temp-path=/var/cache/nginx/proxy_temp \
    --http-fastcgi-temp-path=/var/cache/nginx/fastcgi_temp \
    --http-uwsgi-temp-path=/var/cache/nginx/uwsgi_temp \
    --http-scgi-temp-path=/var/cache/nginx/scgi_temp  --user=nginx  --group=nginx \
    --with-ld-opt="-Wl,-rpath,/usr/local/lib"  \
    --add-module=/root/ngx_devel_kit-0.3.0  \
    --add-module=/root/lua-nginx-module-0.10.7  \
    --without-http_gzip_module \
    && make -j 2 \
    && make install

RUN mkdir -p /var/cache/nginx/ \
    && useradd nginx \
    && mkdir -p /etc/nginx/nginx-lua-prometheus /etc/nginx/conf.d/ \
    && cd /etc/nginx/nginx-lua-prometheus \
    && wget https://raw.githubusercontent.com/knyar/nginx-lua-prometheus/master/prometheus.lua

COPY . /prometheus-nginx

RUN cp /prometheus-nginx/nginx.conf /etc/nginx/nginx.conf \
    && cp /prometheus-nginx/metrics.conf /etc/nginx/conf.d/metrics.conf \
    && chmod +x /prometheus-nginx/entrypoint.sh

EXPOSE 80 9145

ENTRYPOINT ["/prometheus-nginx/entrypoint.sh"]
CMD ["/usr/sbin/nginx"]

