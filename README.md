# Nginx with Prometheus metrics

## Lua

Prometheus metric library for Nginx

### USAGE

```bash
docker run -d -p 80:80 -p 9145:9145 --name prometheus-nginx prometheus-nginx:lua
http://localhost:9145/metrics
```


### Library

* [nginx-lua-prometheus](https://github.com/knyar/nginx-lua-prometheus)

### Reference Document

* [how to install Nginx lua module for Prometheus](http://www.alexlinux.com/prometheus-nginx-monitoring-example/)


## VTS

VTS statistics module for Nginx server

### USAGE

```bash
docker run -d -p 80:80 --name prometheus-nginx prometheus-nginx:vts
curl http://localhost:80/status/format/json
```

### VTS statistics module

* [Nginx virtual host traffic status module](https://github.com/vozlt/nginx-module-vts)

### Reference Document

* [Monitoring NGINX](https://docs.gitlab.com/ce/user/project/integrations/prometheus_library/nginx.html)

### VTS exporter

* [nginx-vts-exporter]https://github.com/hnlq715/nginx-vts-exporter

#### USAGE

```bash
docker run -d -p 9913:9913 --env NGINX_STATUS="http://localhost:80/status/format/json" --name nginx-vts-exporter sophos/nginx-vts-exporter
curl http://localhost:9913/metrics
```
